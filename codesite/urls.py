from django.conf.urls import url
from django.views.generic import ListView, DetailView

from codesite import views
from models import Problems

urlpatterns = [
    url(r'^$', ListView.as_view(queryset=Problems.objects.all().order_by("-created")
                                ,template_name="index.html")),
    url(r'^(?P<pk>\d+)$', DetailView.as_view(
        model=Problems, template_name="problem.html")),
]