# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Problems(models.Model):
    id = models.BigAutoField(db_column='ID', primary_key=True)
    title = models.CharField(max_length=255)
    desc = models.TextField()
    source = models.CharField(max_length=100)
    input_case = models.TextField()
    output_case = models.TextField()
    author = models.CharField(max_length=100)
    problem_type = models.CharField(max_length=255)
    problem_level = models.IntegerField()
    created = models.DateTimeField()

    def __unicode__(self):
        return self.title


class CodeSiteUser(models.Model):
    id = models.BigAutoField(db_column='ID', primary_key=True)
    user_name = models.CharField(max_length=255)
    email = models.EmailField()
    name = models.CharField(max_length=255)
    password = models.CharField(max_length=255)
    problem_solved = models.ForeignKey(Problems, on_delete=models.CASCADE)
    profile_score = models.IntegerField
    created = models.DateTimeField()

    def __unicode__(self):
        return self.user_name

